
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceWarehouse
{
    /// <summary>
    /// 【类】SystemGood,【描述】系统商品
    /// </summary> 
    public class Shop 
    {
        
        /// <summary>
        /// 【属性】shzh,【描述】商户账号（地区编码（6位） + 流水号（8位））
        /// </summary>     
        public string shzh {get;set;}

        /// <summary>
        /// 【属性】shsj,【描述】商户手机
        /// </summary>     
        public string shsj {get;set;}

        /// <summary>
        /// 【属性】shmc,【描述】商户名称
        /// </summary>     
        public string shmc {get;set;}

        /// <summary>
        /// 【属性】dplx,【描述】店铺类型
        /// </summary>     
        public string dplx {get;set;}

        /// <summary>
        /// 【属性】shdz,【描述】商户地址
        /// </summary>     
        public string shdz {get;set;}

        /// <summary>
        /// 【属性】zsxm,【描述】真实姓名
        /// </summary>     
        public string zsxm {get;set;}

        /// <summary>
        /// 【属性】lxsj,【描述】联系手机
        /// </summary>     
        public string lxsj {get;set;}

        /// <summary>
        /// 【属性】lxdh,【描述】联系电话
        /// </summary>     
        public string lxdh {get;set;}

        /// <summary>
        /// 【属性】bydh,【描述】备用电话
        /// </summary>     
        public string bydh {get;set;}

        /// <summary>
        /// 【属性】wxh,【描述】微信号
        /// </summary>     
        public string wxh {get;set;}

        /// <summary>
        /// 【属性】qq,【描述】QQ
        /// </summary>     
        public string qq {get;set;}

    }
}