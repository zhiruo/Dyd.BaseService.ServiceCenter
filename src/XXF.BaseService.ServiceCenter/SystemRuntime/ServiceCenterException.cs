﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.Extensions;

namespace XXF.BaseService.ServiceCenter.SystemRuntime
{
    /// <summary>
    /// 服务中心错误
    /// </summary>
    public class ServiceCenterException:Exception
    {
        public override string Message
        {
            get
            {
                if (this.InnerException != null && !string.IsNullOrWhiteSpace(this.InnerException.Message))
                    return base.Message + "[内部错误:" + this.InnerException.Message.NullToEmpty() + "]";
                return base.Message;
            }
        }
        public ServiceCenterException(string message):base(message)
        {
 
        }

        public ServiceCenterException(string message,Exception exp): base(message, exp)
        {
           
        }
    }
}
