using System;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Data;
using System.Text;
using XXF.Extensions;
using XXF.Db;
using XXF.BaseService.ServiceCenter.Model;

namespace XXF.BaseService.ServiceCenter.Dal
{
    /*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
    public partial class tb_client_dal
    {
        public virtual bool Add(DbConn PubConn, tb_client_model model)
        {

            List<ProcedureParameter> Par = new List<ProcedureParameter>()
                {
					
					//
					new ProcedureParameter("@serverid",    model.serverid),
					//会话id
					new ProcedureParameter("@sessionid",    model.sessionid),
					//项目名
					new ProcedureParameter("@projectname",    model.projectname),
					//ip地址
					new ProcedureParameter("@ip",    model.ip),
                    ////客户端心跳时间
                    //new ProcedureParameter("@clientheartbeattime",    model.clientheartbeattime),
                    ////创建时间
                    //new ProcedureParameter("@createtime",    model.createtime)   
                };
            int rev = PubConn.ExecuteSql(@"insert into tb_client(serverid,sessionid,projectname,ip,clientheartbeattime,createtime)
										   values(@serverid,@sessionid,@projectname,@ip,getdate(),getdate())", Par);
            return rev == 1;

        }

        public virtual bool Edit(DbConn PubConn, tb_client_model model)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>()
            {
                    
					//
					new ProcedureParameter("@serverid",    model.serverid),
					//会话id
					new ProcedureParameter("@sessionid",    model.sessionid),
					//项目名
					new ProcedureParameter("@projectname",    model.projectname),
					//ip地址
					new ProcedureParameter("@ip",    model.ip),
                    ////客户端心跳时间
                    //new ProcedureParameter("@clientheartbeattime",    model.clientheartbeattime),
                    ////创建时间
                    //new ProcedureParameter("@createtime",    model.createtime)
            };
            //Par.Add(new ProcedureParameter("@id", model.id));

            int rev = PubConn.ExecuteSql("update tb_client set serverid=@serverid,projectname=@projectname,ip=@ip,clientheartbeattime=getdate() where sessionid=@sessionid", Par);
            return rev == 1;

        }

        public virtual bool Delete(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));

            string Sql = "delete from tb_client where id=@id";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual bool Delete(DbConn PubConn, long sessionid)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@sessionid", sessionid));

            string Sql = "delete from tb_client where sessionid=@sessionid";
            int rev = PubConn.ExecuteSql(Sql, Par);
            if (rev == 1)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public virtual tb_client_model Get(DbConn PubConn, long sessionid)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@sessionid", sessionid));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_client s where s.sessionid=@sessionid");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_client_model Get(DbConn PubConn, int id)
        {
            List<ProcedureParameter> Par = new List<ProcedureParameter>();
            Par.Add(new ProcedureParameter("@id", id));
            StringBuilder stringSql = new StringBuilder();
            stringSql.Append(@"select s.* from tb_client s where s.id=@id");
            DataSet ds = new DataSet();
            PubConn.SqlToDataSet(ds, stringSql.ToString(), Par);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return CreateModel(ds.Tables[0].Rows[0]);
            }
            return null;
        }

        public virtual tb_client_model CreateModel(DataRow dr)
        {
            var o = new tb_client_model();

            //
            if (dr.Table.Columns.Contains("id"))
            {
                o.id = dr["id"].Toint();
            }
            //
            if (dr.Table.Columns.Contains("serverid"))
            {
                o.serverid = dr["serverid"].Toint();
            }
            //会话id
            if (dr.Table.Columns.Contains("sessionid"))
            {
                o.sessionid = dr["sessionid"].Tolong();
            }
            //项目名
            if (dr.Table.Columns.Contains("projectname"))
            {
                o.projectname = dr["projectname"].Tostring();
            }
            //ip地址
            if (dr.Table.Columns.Contains("ip"))
            {
                o.ip = dr["ip"].Tostring();
            }
            //客户端心跳时间
            if (dr.Table.Columns.Contains("clientheartbeattime"))
            {
                o.clientheartbeattime = dr["clientheartbeattime"].ToDateTime();
            }
            //创建时间
            if (dr.Table.Columns.Contains("createtime"))
            {
                o.createtime = dr["createtime"].ToDateTime();
            }
            return o;
        }
    }
}