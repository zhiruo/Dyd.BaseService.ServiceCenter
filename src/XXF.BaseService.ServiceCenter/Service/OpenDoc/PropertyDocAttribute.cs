﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BaseService.ServiceCenter.Service.OpenDoc
{
    /// <summary>
    /// 实体类属性文档特性
    /// </summary>
    public class PropertyDocAttribute : OpenDocAttribute
    {
        public PropertyDocAttribute() { }
        public PropertyDocAttribute(string text,string description):base(text,description)
        {
 
        }
    }
}
