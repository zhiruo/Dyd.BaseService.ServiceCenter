﻿using Dyd.BaseService.ServiceCenter.Web.Models;
using System.Web;
using System.Web.Mvc;

namespace Dyd.BaseService.ServiceCenter.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogExceptionAttribute());
        }
    }
}