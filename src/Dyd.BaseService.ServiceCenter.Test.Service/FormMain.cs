﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using XXF.BaseService.ServiceCenter.Service;
using XXF.BaseService.ServiceCenter.Service.Provider;
using XXF.Db;

namespace Dyd.BaseService.ServiceCenter.Test.Service
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }
        protected ServiceProvider Service;
        private void button1_Click(object sender, EventArgs e)
        {
            XXF.Common.XXFConfig.ProjectName = "";
            System.Threading.Tasks.Task.Factory.StartNew(() =>
           {
               try
               {
                   Service = new ServiceProvider();
                   Service.RegisterProtocol(typeof(IMyService));
                   Service.Run<MyService>(() =>
                   {
                       this.richTextBox1.Invoke(new Action(() =>
                       {
                           this.richTextBox1.AppendText("开启成功");
                       }));

                       XXF.ProjectTool.SqlHelper.ExcuteSql(XXF.BaseService.ServiceCenter.SystemRuntime.SystemConfigHelper.ServiceCenterConnectString, (c) =>
                       {
                           List<ProcedureParameter> Par = new List<ProcedureParameter>()
                                {
                    //协议json
                    new ProcedureParameter("@protocoljson",  Newtonsoft.Json.JsonConvert.SerializeObject( Service.Protocal))
                                };
                           Par.Add(new ProcedureParameter("@id", 1));

                           int rev = c.ExecuteSql("update tb_protocolversion set protocoljson=@protocoljson where id=@id", Par);
                       });
                   });

               }
               catch (Exception exp)
               {
                   this.richTextBox1.Invoke(new Action(() =>
                   {
                       this.richTextBox1.AppendText(exp.Message);
                   }));
               }
           });


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Service != null)
                Service.Dispose();
        }
    }
}
